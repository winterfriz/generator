from templates import PSQL
from yaml import load

__all__ = ['Generator', 'SchemaError']


class SchemaError(Exception):
    pass


class Generator(object):

    def __init__(self):
        self.tables = []
        self.relations = []
        self.m2m_tables = set()
        self.m2m_relations = set()
        self.functions_and_triggers = []

    def parse_yaml(self, file_name):
        with open(file_name, 'r') as yaml_source:
            self.dump = load(yaml_source.read())

    def generate(self):
        for schema in self.dump:
            table = schema.lower()
            schema_fields = self.dump[schema]['fields']
            table_fields = []

            for field, field_type in schema_fields.items():
                table_fields.append(PSQL.TABLE_FIELD.format(
                    table=table, field=field, field_type=field_type))
            self.tables.append(
                PSQL.TABLE.format(table=table, fields=''.join(table_fields)))

            self._build_rel_statements(schema)

            self.functions_and_triggers.append((
                PSQL.TRIGGER_FUNCTION.format(table=table),
                PSQL.TRIGGER.format(table=table)
            ))

    def _build_rel_statements(self, curr_schema):
        if not 'relations' in self.dump[curr_schema]:
            return

        curr_rels = self.dump[curr_schema]['relations']
        for other_schema, other_rel in curr_rels.items():
            other_rels = self.dump[other_schema]['relations']
            curr_rel = other_rels[curr_schema]

            if not other_schema in self.dump or \
               not curr_schema in other_rels:
                raise SchemaError()

            if other_rel == 'one' and curr_rel == 'many':
                self._build_one2many(curr_schema, other_schema)
            elif other_rel == 'many' and curr_rel == 'many':
                self._build_many2many(curr_schema, other_schema)

    def _build_one2many(self, curr_schema, other_schema):
        self.relations.append(PSQL.RELATION.format(
            left_table=curr_schema.lower(),
            right_table=other_schema.lower()
        ))

    def _build_many2many(self, curr_schema, other_schema):
        names = sorted((curr_schema.lower(), other_schema.lower()))
        self.m2m_tables.add(PSQL.M2M_TABLE.format(
            left_table=names[0], right_table=names[1]))
        self.m2m_relations.add(PSQL.M2M_RELATION.format(
            left_table=names[0], right_table=names[1]))

    def build_statements(self):
        with open('statements.sql', 'w') as out:
            comment = '\n-- {}\n\n'

            out.write(comment.format('tables'))
            out.write(''.join(sorted(self.m2m_tables)))
            out.write(''.join(sorted(self.tables)))

            out.write(comment.format('relations'))
            out.write(''.join(sorted(self.m2m_relations)))
            out.write(''.join(sorted(self.relations)))

            out.write(comment.format('functions and triggers'))
            out.write(''.join(sorted(['{}{}'.format(func, trig)
                                      for func, trig in self.functions_and_triggers])))
