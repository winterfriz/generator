from engine import Generator

if __name__ == "__main__":
    gen = Generator()
    gen.parse_yaml('schema.yaml')
    gen.generate()
    gen.build_statements()
