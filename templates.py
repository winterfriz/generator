__all__ = ['PSQL']


class PSQL(object):

    TABLE = """CREATE TABLE \"{table}\" (
    \"{table}_id\" SERIAL PRIMARY KEY,
{fields}    \"{table}_created\" INTEGER NOT NULL DEFAULT cast(extract(epoch from now()) AS INTEGER),
    \"{table}_updated\" INTEGER NOT NULL DEFAULT cast(extract(epoch from now()) AS INTEGER)
);

"""

    # insert in TABLE in {2}
    TABLE_FIELD = '    \"{table}_{field}\" {field_type} NOT NULL,\n'

    # table with many_to_many relations
    M2M_TABLE = """CREATE TABLE "{left_table}__{right_table}" (
    "{left_table}_id" INTEGER NOT NULL,
    "{right_table}_id" INTEGER NOT NULL,
    PRIMARY KEY ("{left_table}_id", "{right_table}_id")
);

"""

    RELATION = """ALTER TABLE "{left_table}" ADD "{right_table}_id" INTEGER NOT NULL,
    ADD CONSTRAINT "fk_{left_table}_{right_table}_id" FOREIGN KEY ("{right_table}_id") REFERENCES "{right_table}" ("{right_table}_id");

"""

    M2M_RELATION = """ALTER TABLE "{left_table}__{right_table}"
    ADD CONSTRAINT "fk_{left_table}__{right_table}_{left_table}_id" FOREIGN KEY ("{left_table}_id") REFERENCES "{left_table}" ("{left_table}_id");

ALTER TABLE "{left_table}__{right_table}"
    ADD CONSTRAINT "fk_{left_table}__{right_table}_{right_table}_id" FOREIGN KEY ("{right_table}_id") REFERENCES "{right_table}" ("{right_table}_id");

"""

    TRIGGER_FUNCTION = """CREATE OR REPLACE FUNCTION update_{table}_timestamp()
RETURNS TRIGGER AS $$
BEGIN
    NEW.{table}_updated = cast(extract(epoch from now()) as integer);
    RETURN NEW;
END;
$$ language 'plpgsql';
"""

    TRIGGER = ('CREATE TRIGGER tr_{table}_updated '
               'BEFORE UPDATE ON {table} '
               'FOR EACH ROW '
               'EXECUTE PROCEDURE update_{table}_timestamp();\n\n')

if __name__ == "__main__":
    print(PSQL.TABLE)
    print(PSQL.M2M_TABLE)
    print(PSQL.TRIGGER_FUNCTION)
    print(PSQL.FOREIGN_KEY)
    print(PSQL.M2M_FOREIGN_KEYS)
    print(PSQL.TRIGGER)
